app.controller('mainCtrl', function($scope, $state, spaceFactory, formatterService, $modal, apiService,$stateParams, $timeout ){

    $scope.formatter = formatterService;
    $scope.sp = {spaceSearch : ''};
    $scope.userProfile = {};
    $scope.activePage = 0;
    $scope.msg = {privateMessage : ''};
    $scope.msgCount = 0;
    $scope.spaceFactory = spaceFactory;
	$scope.initiatives = [];
	$scope.spaces = [];
	$scope.ideas = [];
	$scope.followers = [];
	$scope.following = [];

    $scope.loadInitiatives = function()
    {
        apiService.get('initiative').then(
            function(res){
                $scope.initiatives = res;
                $scope.spaceFactory.spaceList(res);

            })
    }
    $scope.loadSpaces = function()
    {
        apiService.get('space').then(
            function(res){
                $scope.spaces = res;
                $scope.spaceFactory.spaceList(res);

            })
    }
    $scope.loadIdeas = function()
    {
        apiService.get('idea').then(
            function(res){
                $scope.ideas = res;
                $scope.spaceFactory.spaceList(res);

            })
    }
	
    $scope.loadFollowers = function()
    {
        apiService.get('followers').then(
            function(res){
                $scope.followers = res;
                $scope.spaceFactory.spaceList(res);

            })
    }
	
    $scope.loadFollowing = function()
    {
        apiService.get('following').then(
            function(res){
                $scope.following = res;
                $scope.spaceFactory.spaceList(res);

            })
    }

    $scope.gotoIniciative = function(code)
    {
        $state.go('initiative', {initiativeCode: code});
    };

    $scope.gotoIdea = function(code)
    {
        $state.go('idea', {ideaCode: code});
    };
	
    $scope.followMe = function(id)
    {
        apiService.post('follow', {user_id: id,status:"follow"}).then(
            function(res){
                $scope.msg = {privateMessage : res};
				window.location.reload();
            }
        )
	}
	
    $scope.unfollowMe = function(id)
    {
        apiService.post('follow', {user_id: id,status:"unfollow"}).then(
            function(res){
                $scope.msg = {privateMessage : res};
				window.location.reload();
            }
        )
	}
	
/* 	$scope.userid =  $stateParams['userid'];
	
    apiService.get(['profile/initiatives',  27]).then(
        function(res){
            $scope.contents = res;
        }
    ) */



    $scope.loadInitiatives();
    //$scope.loadSpaces();
    $scope.loadIdeas();
    $scope.loadFollowers();
    $scope.loadFollowing();
	
    $scope.search = { text : ''};

    $scope.doSearch = function()

    {

        if($scope.search.text != ''){
            $state.go('search', {query: $scope.search.text});
            $scope.search = { text : ''};
        }
    }

    $scope.sendMessageClick = function(){

        if($scope.msg.privateMessage == '') return;

        apiService.post('message', {to_id: $scope.userProfile.id, body: $scope.msg.privateMessage}).then(
            function(res){
                $scope.msg = {privateMessage : ''};

            }
        )
    }

    $scope.$on('peopleDialogEvent', function(e,id){

        apiService.getCached(['profile', id], null, true).then(
            function(res){
                $scope.userProfile = res;

                var peopleModal = $modal({template: 'partials/modal-people.html', scope: $scope, animation: 'am-fade-and-slide-top', show: true});

            }
        )
    })

    $scope.$on('messageCountChanged', function(){
        $scope.mainPool();
    })
	

    $scope.gotoSpace = function(code)
    {
        $scope.spaceSearch = '';
        $timeout(function(){
            $scope.sp = {spaceSearch : ''};

            $state.go('space.stream', {spaceCode: code});
        }, 200);
    }


    $scope.gotoPage = function(page)
    {
        $timeout(function(){
            $state.go(page);
        });
    }

    $scope.$on('initiativeListChanged', function(e){
        $scope.loadInitiatives();
    });
	
    $scope.$on('ideaListChanged', function(e){
        $scope.loadIdeas();
    });

    $scope.getLogoutUrl = function()
    {
        return siteUrl + '/logout';
    }


    $scope.mainPool = function()
    {
        apiService.get('mainpool').then(
            function(res)
            {

                if(res.msgs > $scope.msgCount)
                    sndBell.play();

                $scope.msgCount = res.msgs;

                $timeout($scope.mainPool, 60000);
            }
        )

    }

    $scope.mainPool();
});