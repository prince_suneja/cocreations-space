app.controller('homeCtrl', function($scope, $state,$cacheFactory, $timeout, spaceFactory, formatterService, $modal, apiService ){
    $scope.formatter = formatterService;

    $scope.features = [
        {value: 'drive', text: translations['DRIVE']},
        {value:'wikis', text:translations['WIKIS']},
        {value:'tasks', text: translations['TASKS']},
        {value:'chat', text: translations['CHAT']},
    ];
    $scope.spaces = [];

    $scope.createSpaceClick = function()
    {
        $scope.editSpace = {title: "", description: '', options:{ features: ['drive', 'wikis', 'tasks', 'chat']}, access: 'PU'};
        $scope.error = null;
        $scope.spaceForm = $modal({template: 'partials/modal-space-edit.html', title: 'New project', scope: $scope, animation: 'am-fade-and-slide-top', show: true});
    }
	
    $scope.createIdeaClick = function()
    {
        $scope.editIdea = {title: "", description: '', purpose_of_idea: '', idea_video: '', idea_paper: '', stpes_of_development: '', planning_estimation:'',options:{ features: ['drive', 'wikis', 'tasks', 'chat']}, access: 'PU', terms:''};
        $scope.error = null;
        $scope.ideaForm = $modal({template: 'partials/modal-idea-edit.html', title: 'New idea', scope: $scope, animation: 'am-fade-and-slide-top', show: true});
    }
	
    $scope.createInitiativeClick = function()
    {
        $scope.editInitiative = {title: "", description: '', purpose_of_initiative: '', initiative_video: '', initiative_paper: '', stpes_of_development: '', planning_estimation:'', options:{ features: ['drive', 'wikis', 'tasks', 'chat']}};
        $scope.error = null;
        $scope.initiativeForm = $modal({template: 'partials/modal-initiative-edit.html', title: 'New Initiative', scope: $scope, animation: 'am-fade-and-slide-top', show: true});
    }

    $scope.canCreateSpace = function()
    {
        return user.cs == 1;
    }

    $scope.editSpaceOkClick = function()
    {
        apiService.post(['space'], $scope.editSpace).then(
            function(res){
                $scope.spaceForm.hide();
                $scope.$emit('spaceListChanged');
                //document.location.href= '#/space/'+res;
                $state.go('space.stream', {spaceCode: res});

            }, function(err){
                $scope.error = err;
            }
        )
    }
	
    $scope.editInitiativeOkClick = function()
    { 
        apiService.post(['initiative'], $scope.editInitiative).then(
            function(res){
                $scope.initiativeForm.hide();
                $scope.$emit('initiativeListChanged');
                //document.location.href= '#/initiative/'+res;
                $state.go('initiative', {initiativeCode: res});

            }, function(err){
                $scope.error = err;
            }
        )
    }
	
    $scope.editIdeaOkClick = function()
    { 
        apiService.post(['idea'], $scope.editIdea).then(
            function(res){
                $scope.ideaForm.hide();
                $scope.$emit('ideaListChanged');
                document.location.href= '#/idea/'+res;
                //$state.go('idea.stream', {ideaCode: res});

            }, function(err){
                $scope.error = err;
            }
        )
    }
	
    $scope.onFileSelect = function($files){

        var file = $files[0];
        usSpinnerService.spin('avatarSpinner');

        $upload.upload({
            url: apiUrl + '/profile/avatar',
            file: file})
            .success(function(data, status, headers, config) {

                $timeout(function() {
                    var av = Math.floor((Math.random() * 1000000) + 1);

                    $scope.profile.avatar = $scope.avatar = formatterService.getAvatar($scope.profile.id)+'?'+av;
                    usSpinnerService.stop('avatarSpinner');
                    //location.reload(true);
                },1000)
            }).error(function(){
                usSpinnerService.stop('avatarSpinner');

            })
    }

    $scope.zoomClick = function(contentId){
       $state.go('post', {contentId: contentId});
    }

    $scope.testClick = function()
    {
        spaceFactory.loadSpaces().then(
            function(res){
                $scope.activity = res;
            });

    }

    $scope.joinClick = function(code){

        apiService.put(['space', code, 'join']).then(
            function(){
                $state.go('space.stream', {spaceCode: code});
                $scope.$emit('spaceListChanged');
            });
    }

    $scope.icons = [ 'fa fa-comment', 'fa fa-link', 'fa fa-table', 'fa fa-bar-chart', 'fa fa-check-square', 'fa fa-calendar', 'fa fa-map-marker'];

    $scope.getLogoutUrl = function()
    {
        return siteUrl + '/logout';
    }

    $scope.gotoSpace = function(code)
    {
        $timeout(function(){
            $state.go('space.stream', {spaceCode: code});

        });
    }

        $scope.loadSpaces = function()
    {
        apiService.get('home').then(
            function(res){
                var color = $('.btn-info').css( "background-color" );
                $scope.spaces = res.spaces;
                $scope.join = res.join;
                $timeout(function(){
                    angular.forEach(res.spaces, function(space){
                        //8ED2F3
                        $('#spark_'+space.code).sparkline(space.counter, {disableTooltips: true, disableHighlight: true, type: 'bar',
                            barColor: color,
                            height:'40px', barWidth: '15', barSpacing:'2'} );

                    });


                })
            }
        )
    }

    $scope.loadContent = function () {
        apiService.get('home/content').then(
            function (res) {
                $scope.contents = res;
            }
        )
    }

    $scope.events = [];
    if(!h_cal) {
        apiService.get(['event'], {view: 'short'}).then(
            function (res) {
                $scope.events = res;
            }
        )
    }

    $scope.tasks = null;

    apiService.get('task/counters').then(
        function(data){
            $scope.tasks = data;

        }
    )

    $scope.loadSpaces();
   $scope.loadContent();

})
